package com.example.demo;

import com.example.demo.dao.EmployeeRepository;
import com.example.demo.entities.Employee;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class QueryServiceTest {

    @Mock
    EmployeeRepository employeeRepository;

    private QueryService service;

    @Before
    public void setUp() {
        service = new QueryService(employeeRepository);
    }

    @Test
    public void testSettingAnOption() {
        Map<String, String> options = new HashMap<>();

        options.put("payment", "Hourly");
        service.setPayment("Hourly");
        assertThat(service.getSelectedOptions()).isEqualTo(options);

        options.put("department", "New Department");
        service.setDepartment("New Department");
        assertThat(service.getSelectedOptions()).isEqualTo(options);

        options.put("education", "Test Education");
        service.setEducation("Test Education");
        assertThat(service.getSelectedOptions()).isEqualTo(options);
    }

    @Test
    public void testQueryRun() {
        Employee alex = new Employee();
        alex.setEducationLevel("Test Education");

        List<Employee> employees = new ArrayList<>();
        employees.add(alex);

        when(employeeRepository.findAllFilteredByDepartmentEducationPayment("", "", ""))
                .thenReturn(employees);

        assertThat(service.run()).isEqualTo(employees);
    }
}

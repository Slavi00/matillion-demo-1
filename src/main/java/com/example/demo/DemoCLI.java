package com.example.demo;

import com.example.demo.entities.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import org.springframework.shell.table.*;

import java.util.LinkedHashMap;

@ShellComponent
public class DemoCLI {

    private final QueryService query;

    @Autowired
    public DemoCLI(QueryService query) {
        this.query = query;
    }

    @ShellMethod("Set department, education and payment options")
    public void set(@ShellOption(defaultValue = "") String department,
                    @ShellOption(defaultValue = "") String education,
                    @ShellOption(defaultValue = "") String payType) {
        if (!department.equals("")) { query.setDepartment(department); }
        if (!education.equals("")) { query.setEducation(education); }
        if (!payType.equals("")) { query.setPayment(payType); }
    }

    @ShellMethod("Make a query with department, education, payment options")
    public Table queryEmployees() {
        Iterable<Employee> employees = query.run();
        return buildEmployeesTable(employees);
    }

    @ShellMethod("Clear options")
    public String clearOptions() {
        query.setDepartment("");
        query.setEducation("");
        query.setPayment("");
        return "Options cleared";
    }

    private Table buildEmployeesTable(Iterable<Employee> employees) {
        LinkedHashMap<String, Object> headers = new LinkedHashMap<>();
        headers.put("fullName", "Name");
        headers.put("positionTitle", "Position");
        headers.put("position.minScale", "Min Scale");
        headers.put("position.maxScale", "Max Scale");
        headers.put("position.payType", "Pay Type");
        headers.put("storeId", "Store Id");
        headers.put("birthDate", "Birth Date");
        headers.put("hireDate", "Hire Date");
        headers.put("salary", "Salary");
        headers.put("supervisorId", "Supervisor Id");
        headers.put("educationLevel", "Education");
        headers.put("maritalStatus", "Marital Status");
        headers.put("gender", "Gender");
        headers.put("managementRole", "Role");
        return buildTable(employees, headers);
    }

    private Table buildTable(Iterable<Employee> employees, LinkedHashMap<String, Object> headers) {
        TableModel model = new BeanListTableModel<>(employees, headers);
        TableBuilder tableBuilder = new TableBuilder(model);
        applyStyle(tableBuilder);
        return tableBuilder.build();
    }

    public TableBuilder applyStyle(TableBuilder builder) {
        builder.addOutlineBorder(BorderStyle.fancy_double)
                .paintBorder(BorderStyle.air, BorderSpecification.INNER_VERTICAL)
                .fromTopLeft().toBottomRight()
                .paintBorder(BorderStyle.fancy_light, BorderSpecification.INNER_VERTICAL)
                .fromTopLeft().toBottomRight()
                .addHeaderBorder(BorderStyle.fancy_double)
                .on(CellMatchers.row(0))
                .addAligner(SimpleVerticalAligner.middle)
                .addAligner(SimpleHorizontalAligner.center)
        ;
        return Tables.configureKeyValueRendering(builder, " = ");
    }
}
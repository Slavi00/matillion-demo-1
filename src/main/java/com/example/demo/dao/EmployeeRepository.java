package com.example.demo.dao;

import com.example.demo.entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    @Query("from Employee e" +
            " where e.department.departmentDescription like %:department%" +
            " and e.educationLevel like %:education%" +
            " and e.position.payType like %:payment%")
    Iterable<Employee> findAllFilteredByDepartmentEducationPayment(String department, String education, String payment);
}

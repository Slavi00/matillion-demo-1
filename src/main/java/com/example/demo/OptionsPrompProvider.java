package com.example.demo;

import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.jline.PromptProvider;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
class OptionsPromptProvider implements PromptProvider {
    private final QueryService query;

    @Autowired
    OptionsPromptProvider(QueryService query) {
        this.query = query;
    }

    @Override
    public AttributedString getPrompt() {
        String selected = query.getSelectedOptions().entrySet()
                .stream()
                .map(option -> option.getKey() + ": " + option.getValue())
                .collect(Collectors.joining(", "));

        return new AttributedString(selected + "\nfoodmart > ",
                AttributedStyle.DEFAULT.foreground(AttributedStyle.YELLOW));
    }
}
package com.example.demo.entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "position", schema = "foodmart")
public class Position {
    @Id
    private int positionId;
    private String positionTitle;
    private String payType;
    private BigDecimal minScale;
    private BigDecimal maxScale;
    private String managementRole;

    @OneToMany(mappedBy = "position")
    private Set<Employee> employees;

    @Basic
    @Column(name = "position_id", nullable = false)
    public int getPositionId() {
        return positionId;
    }

    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }

    @Basic
    @Column(name = "position_title", nullable = false, length = 30)
    public String getPositionTitle() {
        return positionTitle;
    }

    public void setPositionTitle(String positionTitle) {
        this.positionTitle = positionTitle;
    }

    @Basic
    @Column(name = "pay_type", nullable = false, length = 30)
    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    @Basic
    @Column(name = "min_scale", nullable = false, precision = 4)
    public BigDecimal getMinScale() {
        return minScale;
    }

    public void setMinScale(BigDecimal minScale) {
        this.minScale = minScale;
    }

    @Basic
    @Column(name = "max_scale", nullable = false, precision = 4)
    public BigDecimal getMaxScale() {
        return maxScale;
    }

    public void setMaxScale(BigDecimal maxScale) {
        this.maxScale = maxScale;
    }

    @Basic
    @Column(name = "management_role", nullable = false, length = 30)
    public String getManagementRole() {
        return managementRole;
    }

    public void setManagementRole(String managementRole) {
        this.managementRole = managementRole;
    }

    public Set<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position that = (Position) o;
        return positionId == that.positionId &&
                Objects.equals(positionTitle, that.positionTitle) &&
                Objects.equals(payType, that.payType) &&
                Objects.equals(minScale, that.minScale) &&
                Objects.equals(maxScale, that.maxScale) &&
                Objects.equals(managementRole, that.managementRole);
    }

    @Override
    public int hashCode() {
        return Objects.hash(positionId, positionTitle, payType, minScale, maxScale, managementRole);
    }
}

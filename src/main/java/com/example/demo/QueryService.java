package com.example.demo;

import com.example.demo.dao.EmployeeRepository;
import com.example.demo.entities.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class QueryService {

    private final EmployeeRepository employeeRepository;

    private String department;
    private String education;
    private String payment;

    @Autowired
    public QueryService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;

        department = education = payment = "";
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public Iterable<Employee> run() {
        return employeeRepository.findAllFilteredByDepartmentEducationPayment(
                department, education, payment);
    }

    public Map<String, String> getSelectedOptions() {
        Map<String, String> options = new HashMap<>();
        if (!department.equals("")) {
            options.put("department", department);
        }
        if (!education.equals("")) {
            options.put("education", education);
        }
        if (!payment.equals("")) {
            options.put("payment", payment);
        }
        return options;
    }
}

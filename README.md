# Matillion-demo-1

Command line Java program that allows the user to specify a department, pay type and education
level, and then connects to the shared database and runs the query with those options. The program then displays the results of the query.

### Build and run the demo from CLI
> mvn clean install

> java -jar target/demo-0.0.1-SNAPSHOT.jar